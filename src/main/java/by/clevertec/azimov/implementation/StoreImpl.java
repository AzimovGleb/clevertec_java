package by.clevertec.azimov.implementation;

import by.clevertec.azimov.interfaces.Store;
import by.clevertec.azimov.models.Card;
import by.clevertec.azimov.models.Product;

import java.io.*;
import java.util.*;

public class StoreImpl implements Store {
    private Map<Integer, Product> productsList;
    private List<Card> cardsList;

    public StoreImpl() {
    }

    @Override
    public Map<Integer, Product> readProducts(String fileName) {
        productsList = new HashMap<>();
        cardsList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File(fileName)))) {
            String line = reader.readLine();
            int count = 0;
            int productsCounter = 1;
            while (line != null && count == 0) {
                String[] productParameters = line.split(";");
                productsList.put(productsCounter, new Product(productParameters[0],
                        Double.parseDouble(productParameters[1]), Boolean.parseBoolean(productParameters[2])));
                productsCounter++;
                line = reader.readLine();
                if (line.equals("Cards")) {
                    count++;
                }
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return productsList;
    }

    @Override
    public List<Card> readCards(String fileName) {
        cardsList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File(fileName)))) {
            String line = reader.readLine();
            int count = 0;
            while (line != null) {
                if (line.equals("Cards")) {
                    count++;
                    line = reader.readLine();
                } else if (count != 0) {
                    String[] cardParameters = line.split(";");
                    cardsList.add(new Card(Integer.parseInt(cardParameters[0]), Integer.parseInt(cardParameters[1])));
                    line = reader.readLine();
                } else {
                    line = reader.readLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cardsList;
    }

    @Override
    public Card createCard(String cardNumber, List<Card> cardsList) {
        Card actualCard = new Card();
        for (Card card : cardsList) {
            if (card.getCardNumber() == Integer.parseInt(cardNumber)) {
                actualCard = card;
            }
        }
        return actualCard;
    }
}
