package by.clevertec.azimov.implementation;

import by.clevertec.azimov.interfaces.CheckMaker;
import by.clevertec.azimov.Main;
import by.clevertec.azimov.models.Card;
import by.clevertec.azimov.models.Product;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckMakerImpl implements CheckMaker {
    public final int MIN_COUNT_FOR_DISCOUNT = 6;
    public final double DISCOUNT_COEFFICIENT_FOR_THIS_PRODUCT = 0.9;
    public final String OUTPUT_FILE_NAME = "src\\main\\resources\\Check.txt";

    Logger logger = Logger.getLogger(Main.class.getName());

    @Override
    public String[] createInputMass(String[] args) {
        StringBuilder sB = new StringBuilder();
        for (String arg : args) {
            sB.append(arg + " ");
        }
        if (sB.toString().indexOf("card") < 4) {
            logger.log(Level.WARNING, "String doesn't match format");
        }
        String[] newArgs = sB.toString().split("card");
        String[] cardAndFileName = newArgs[1].split(" ");
        newArgs = new String[]{newArgs[0], cardAndFileName[0], cardAndFileName[1]};
        return newArgs;
    }

    @Override
    public String getActualCardNumber(String s) {
        StringBuilder actualCardNumber = new StringBuilder();
        Pattern patternForNumber = Pattern.compile("\\d");
        Matcher matcherForNumber = patternForNumber.matcher(s);
        while (matcherForNumber.find()) {
            actualCardNumber.append(s, matcherForNumber.start(), matcherForNumber.end());
        }
        return actualCardNumber.toString();
    }

    @Override
    public Map<Integer, Integer> createMapForItemIdAndQuantity(String itemIdQuantityNumbers) {
        if (!Pattern.matches("(\\d-\\d )+", itemIdQuantityNumbers)) {
            logger.log(Level.WARNING, "String doesn't match format");
        }
        Map<Integer, Integer> mapForItemIdAndQuantity = new HashMap<>();
        String[] idAndQtys = itemIdQuantityNumbers.split(" ");
        for (String idAndQty : idAndQtys) {
            String[] idQTY = idAndQty.split("-");
            mapForItemIdAndQuantity.put(Integer.parseInt(idQTY[0]), Integer.parseInt(idQTY[1]));
        }
        return mapForItemIdAndQuantity;
    }

    @Override
    public String round(Double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return String.valueOf(bd);
    }

    @Override
    public Map<Product, Double> createMapWithTotalPrice(Map<Integer, Integer> map, Map<Integer, Product> products) {
        Map<Product, Double> menu = new HashMap<>();
        try {
            Integer[] itemId = map.keySet().toArray(new Integer[0]);
            for (Integer item : itemId) {
                Double price = products.get(item).getPrice();
                Integer quantity = map.get(item);
                Double totalPrice = price * quantity;
                menu.put(products.get(item), Double.valueOf(round(totalPrice, 2)));
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, "Not found this product");
        }
        return menu;
    }

    public void writeCheck(Map<Product, Double> menu, Card card) {

        try (PrintStream printStream = new PrintStream(OUTPUT_FILE_NAME)) {
            double totalDiscountsMoney = 0;
            double totalPriceAll = 0;
            printStream.println(String.format("%46s", "CASH RECEIPT"));
            printStream.println(String.format("%48s", "SUPERMARKET 123"));
            printStream.println(String.format("%52s", "12, MILKYWAY Galaxy/ Earth"));
            printStream.println(String.format("%49s", "Tel :123-456-7890"));
            printStream.println(String.format("%s%10s%47s%td/%tm/%tY", "CASHIER", "№1520", "DATE:", new Date(), new Date(),
                    new Date()));
            printStream.println(String.format("%64s%tH:%tM:%tS", "TIME:", new Date(), new Date(), new Date(), new Date()));
            printStream.println("-------------------------------------------------------------------------------");
            printStream.println(String.format("%2s%24s%33s%14s", "QTI", "DESCRIPTION", "PRICE", "TOTAL"));
            for (Product p : menu.keySet().toArray(new Product[0])) {
                String quantity = round(menu.get(p) / p.getPrice(), 0);
                if (Double.parseDouble(quantity) >= MIN_COUNT_FOR_DISCOUNT && p.isDiscount()) {
                    totalDiscountsMoney = totalDiscountsMoney + (menu.get(p) - (menu.get(p) *
                            DISCOUNT_COEFFICIENT_FOR_THIS_PRODUCT));
                    menu.put(p, menu.get(p) * DISCOUNT_COEFFICIENT_FOR_THIS_PRODUCT);
                }
                totalPriceAll = totalPriceAll + menu.get(p);
                printStream.println(String.format("%2s%24s%30s%s%10s%s", quantity, p.getName(), "$", round(p.getPrice(),
                        2), "$", round(menu.get(p), 2)));
            }
            printStream.println("===============================================================================");
            String discountsPercent = round(((totalDiscountsMoney / totalPriceAll) * 100) + card.getDiscount(),
                    0);
            totalDiscountsMoney = totalDiscountsMoney + (totalPriceAll * (Double.parseDouble(discountsPercent) / 100));
            Double totalPriceAllWithDiscount = totalPriceAll - totalDiscountsMoney;
            printStream.println(String.format("%s%59s%s", "TAXABLE TOT", "$", round(totalPriceAllWithDiscount, 2)));
            printStream.println(String.format("%s%s%s%65s%s", "VAT", discountsPercent, "%", "$", round(totalDiscountsMoney,
                    2)));
            printStream.println(String.format("%s%65s%s", "TOTAL", "$", round(totalPriceAll, 2)));
        } catch (Exception e) {
            logger.log(Level.WARNING, String.valueOf(e));
        } finally {
            logger.log(Level.INFO, "OutputFile located to:" + OUTPUT_FILE_NAME);
        }
    }
}
