package by.clevertec.azimov.interfaces;

import by.clevertec.azimov.models.Card;
import by.clevertec.azimov.models.Product;

import java.util.List;
import java.util.Map;

public interface Store {

    Card createCard(String cardNumber, List<Card> cardsList);

    Map<Integer, Product> readProducts(String fileName);

    List<Card> readCards(String fileName);

}
