package by.clevertec.azimov.interfaces;

import by.clevertec.azimov.models.Product;

import java.util.Map;

public interface CheckMaker {

    String[] createInputMass(String[] s);

    String getActualCardNumber(String s);

    Map<Integer, Integer> createMapForItemIdAndQuantity(String itemIdQuantityNumbers);

    String round(Double value, int places);

    Map<Product, Double> createMapWithTotalPrice(Map<Integer, Integer> map, Map<Integer, Product> products);

}
