package by.clevertec.azimov.models;

public class Product {

    private String name;
    private double price;
    private boolean discount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isDiscount() {
        return discount;
    }

    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

    public Product() {

    }

    public Product(String name, double price, boolean discount) {
        this.name = name;
        this.price = price;
        this.discount = discount;
    }


}
