package by.clevertec.azimov.models;

public class Card {
    private int cardNumber;
    private int discount;

    public Card() {

    }

    public Card(int cardNumber, int discount) {
        this.cardNumber = cardNumber;
        this.discount = discount;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public int getDiscount() {
        return discount;
    }
}
