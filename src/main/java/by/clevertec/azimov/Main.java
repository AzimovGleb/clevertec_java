package by.clevertec.azimov;

import by.clevertec.azimov.implementation.CheckMakerImpl;
import by.clevertec.azimov.implementation.StoreImpl;
import by.clevertec.azimov.interfaces.Store;
import by.clevertec.azimov.models.Card;
import by.clevertec.azimov.models.Product;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Store store = new StoreImpl();
        CheckMakerImpl checkMaker = new CheckMakerImpl();

        String[] newArgs = checkMaker.createInputMass(args);
        Map<Integer, Product> productMap = store.readProducts(newArgs[2]);
        List<Card> cardList = store.readCards(newArgs[2]);
        Card actualCard = store.createCard(checkMaker.getActualCardNumber(newArgs[1]), cardList);
        Map<Integer, Integer> itemsMap = checkMaker.createMapForItemIdAndQuantity(newArgs[0]);
        Map<Product, Double> productsMap = checkMaker.createMapWithTotalPrice(itemsMap, productMap);
        checkMaker.writeCheck(productsMap, actualCard);
    }
}

