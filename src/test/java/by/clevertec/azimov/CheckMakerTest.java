package by.clevertec.azimov;

import by.clevertec.azimov.implementation.CheckMakerImpl;
import by.clevertec.azimov.interfaces.CheckMaker;
import by.clevertec.azimov.models.Product;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CheckMakerTest {

    private final CheckMaker checkCreator = new CheckMakerImpl();

    private static final Map<Integer, Integer> map;
    private static final Map<Integer, Product> productsList;
    private static final Map<Product, Double> totalPriceList;

    static {
        map = new HashMap<>();
        map.put(1, 6);
        map.put(3, 1);
        map.put(2, 6);
        map.put(5, 6);
        map.put(6, 6);

        productsList = new HashMap<>();
        productsList.put(1, new Product("Cookies", 1.15, false));
        productsList.put(2, new Product("Bananas", 2.36, false));
        productsList.put(3, new Product("Milk chocolate", 1.35, true));
        productsList.put(4, new Product("Chickens meat", 4.60, false));
        productsList.put(5, new Product("Oatmeal", 0.50, true));
        productsList.put(6, new Product("Beef", 7.15, false));
        productsList.put(7, new Product("Fish", 8.10, false));
        productsList.put(8, new Product("Cottage cheese", 1.05, true));
        productsList.put(9, new Product("Milk", 0.98, true));
        productsList.put(10, new Product("Bread", 0.86, false));

        totalPriceList = new HashMap<>();
        totalPriceList.put(new Product("Cookies", 1.15, false), 12.23);
        totalPriceList.put(new Product("Bananas", 2.36, false), 14.10);
        totalPriceList.put(new Product("Milk chocolate", 1.35, true), 15.14);
    }

    @Test
    public void testBuilderStringForStartParameters() {
        String[] testString = new String[]{"1-6 3-1 2-6 5-6 6-6 card-1234 src\\main\\resources\\StoreList.csv"};
        testString = checkCreator.createInputMass(testString);

        assertEquals(testString[1], "-1234");
    }

    @Test
    public void testSplitCardNumber() {
        String testString = "-1234 ";
        testString = checkCreator.getActualCardNumber(testString);

        assertEquals(testString, "1234");
    }

    @Test
    public void testCreateMapByItemIdQuantity() {
        String testString = "1-6 3-1 2-6 5-6 6-6 ";
        Map<Integer, Integer> testMap = checkCreator.createMapForItemIdAndQuantity(testString);

        assertEquals(testMap.keySet(), map.keySet());
        assertEquals(testMap.get(3), map.get(3));
    }

    @Test
    public void testRound() {
        String test1 = checkCreator.round(12.23000000, 2);
        String test2 = checkCreator.round(12.23600000, 2);

        assertEquals(test1, "12.23");
        assertEquals(test2, "12.24");
    }

    @Test
    public void testCalculatePrice() {
        Map<Product, Double> testTotalPrice = checkCreator.createMapWithTotalPrice(map, productsList);
        Product testProduct = new Product("Cookies", 1.15, false);

        assertEquals(testTotalPrice.get(testProduct), totalPriceList.get(testProduct));
    }

}
