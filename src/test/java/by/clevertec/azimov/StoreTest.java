package by.clevertec.azimov;

import by.clevertec.azimov.implementation.StoreImpl;
import by.clevertec.azimov.interfaces.Store;
import by.clevertec.azimov.models.Card;
import by.clevertec.azimov.models.Product;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StoreTest {

    Store store = new StoreImpl();

    private static final Map<Integer, Product> productsList;
    private static final List<Card> cardsList;

    static {
        productsList = new HashMap<>();
        productsList.put(1, new Product("Cookies", 1.15, false));
        productsList.put(2, new Product("Bananas", 2.36, false));
        productsList.put(3, new Product("Milk chocolate", 1.35, true));
        productsList.put(4, new Product("Chickens meat", 4.60, false));
        productsList.put(5, new Product("Oatmeal", 0.50, true));
        productsList.put(6, new Product("Beef", 7.15, false));
        productsList.put(7, new Product("Fish", 8.10, false));
        productsList.put(8, new Product("Cottage cheese", 1.05, true));
        productsList.put(9, new Product("Milk", 0.98, true));
        productsList.put(10, new Product("Bread", 0.86, false));

        cardsList = new ArrayList<>();
        cardsList.add(new Card(1234, 5));
        cardsList.add(new Card(1233, 0));
        cardsList.add(new Card(1232, 3));
        cardsList.add(new Card(1231, 0));
        cardsList.add(new Card(1230, 2));
    }

    @Test
    public void testCreateCard() {
        Card testCard = store.createCard("1234", cardsList);

        assertEquals(testCard.getCardNumber(), 1234);
        assertEquals(testCard.getDiscount(), 5);
    }

    @Test
    public void testReadCards() {
        List<Card> testList = store.readCards("src\\main\\resources\\StoreList.csv");

        assertEquals(testList.get(0).getCardNumber(), 1234);
        assertEquals(testList.get(0).getDiscount(), 5);
    }

    @Test
    public void testReadProducts() {
        Map<Integer, Product>  testProductsList = store.readProducts ("src\\main\\resources\\StoreList.csv");

        assertEquals(testProductsList.get(3).getName(), "Milk chocolate");
        assertTrue(testProductsList.get(3).isDiscount());
    }
}
